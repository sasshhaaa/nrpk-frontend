
const ButtonDropdown = () => (
  <>
    <div className="buttonDropdown group inline-block">
      <button
        className="outline-none focus:outline-none border px-3 py-1 bg-white rounded-sm flex items-center min-w-32"
      >
        <span className="pr-1 font-semibold flex-1">Головна</span>
        <span>
          <svg
            className="fill-current h-4 w-4 transform group-hover:-rotate-180
        transition duration-150 ease-in-out"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20"
          >
            <path
              d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"
            />
          </svg>
        </span>
      </button>
      <ul
        className="bg-white border rounded-sm transform scale-0 group-hover:scale-100 absolute
  transition duration-150 ease-in-out origin-top min-w-32"
      >
        <li className="rounded-sm px-3 py-1 hover:bg-gray-100">Публічна інформація</li>
        <li className="rounded-sm px-3 py-1 hover:bg-gray-100">Новини</li>
        <li className="rounded-sm relative px-3 py-1 hover:bg-gray-100">
          <button
            className="w-full text-left flex items-center outline-none focus:outline-none"
          >
            <span className="pr-1 flex-1">Спеціальності</span>
            <span className="mr-auto">
              <svg
                className="fill-current h-4 w-4
            transition duration-150 ease-in-out"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
              >
                <path
                  d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"
                />
              </svg>
            </span>
          </button>
          <ul
            className="bg-white border rounded-sm absolute top-0 right-0
  transition duration-150 ease-in-out origin-top-left
  min-w-32
  "
          >
            <li className="rounded-sm relative px-3 py-1 hover:bg-gray-100">
              <button
                className="w-full text-left flex items-center outline-none focus:outline-none"
              >
                <span className="pr-1 flex-1">АТ</span>
                <span className="mr-auto">
                  <svg
                    className="fill-current h-4 w-4
                transition duration-150 ease-in-out"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                  >
                    <path
                      d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"
                    />
                  </svg>
                </span>
              </button>
              <ul
                className="bg-white border rounded-sm absolute top-0 right-0
      transition duration-150 ease-in-out origin-top-left
      min-w-32
      "
              >
                <li className="px-3 py-1 hover:bg-gray-100">Кадровий склад</li>
                <li className="px-3 py-1 hover:bg-gray-100">ОПП</li>
              </ul>
            </li>
            <li className="rounded-sm relative px-3 py-1 hover:bg-gray-100">
              <button
                className="w-full text-left flex items-center outline-none focus:outline-none"
              >
                <span className="pr-1 flex-1">ЕЕМ</span>
                <span className="mr-auto">
                  <svg
                    className="fill-current h-4 w-4
                transition duration-150 ease-in-out"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                  >
                    <path
                      d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"
                    />
                  </svg>
                </span>
              </button>
              <ul
                className="bg-white border rounded-sm absolute top-0 right-0
      transition duration-150 ease-in-out origin-top-left
      min-w-32
      "
              >
                <li className="px-3 py-1 hover:bg-gray-100">Кадровий склад</li>
                <li className="px-3 py-1 hover:bg-gray-100">ОПП</li>
              </ul>
            </li>
            <li className="rounded-sm relative px-3 py-1 hover:bg-gray-100">
              <button
                className="w-full text-left flex items-center outline-none focus:outline-none"
              >
                <span className="pr-1 flex-1">МК</span>
                <span className="mr-auto">
                  <svg
                    className="fill-current h-4 w-4
                transition duration-150 ease-in-out"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                  >
                    <path
                      d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"
                    />
                  </svg>
                </span>
              </button>
              <ul
                className="bg-white border rounded-sm absolute top-0 right-0
      transition duration-150 ease-in-out origin-top-left
      min-w-32
      "
              >
                <li className="px-3 py-1 hover:bg-gray-100">Кадровий склад</li>
                <li className="px-3 py-1 hover:bg-gray-100">ОПП</li>
              </ul>
            </li>
            <li className="rounded-sm relative px-3 py-1 hover:bg-gray-100">
              <button
                className="w-full text-left flex items-center outline-none focus:outline-none"
              >
                <span className="pr-1 flex-1">КІ</span>
                <span className="mr-auto">
                  <svg
                    className="fill-current h-4 w-4
                transition duration-150 ease-in-out"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                  >
                    <path
                      d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"
                    />
                  </svg>
                </span>
              </button>
              <ul
                className="bg-white border rounded-sm absolute top-0 right-0
      transition duration-150 ease-in-out origin-top-left
      min-w-32
      "
              >
                <li className="px-3 py-1 hover:bg-gray-100">Кадровий склад</li>
                <li className="px-3 py-1 hover:bg-gray-100">ОПП</li>
              </ul>
            </li>

          </ul>
        </li>
        <li className="rounded-sm relative px-3 py-1 hover:bg-gray-100">
          <button
            className="w-full text-left flex items-center outline-none focus:outline-none"
          >
            <span className="pr-1 flex-1">Вступнику</span>
            <span className="mr-auto">
              <svg
                className="fill-current h-4 w-4
                transition duration-150 ease-in-out"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
              >
                <path
                  d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"
                />
              </svg>
            </span>
          </button>
          <ul
            className="bg-white border rounded-sm absolute top-0 right-0
      transition duration-150 ease-in-out origin-top-left
      min-w-32
      "
          >
            <li className="px-3 py-1 hover:bg-gray-100">Правила прийому</li>
            <li className="px-3 py-1 hover:bg-gray-100">Розмір плати за навчання</li>
          </ul>
        </li>
        <li className="rounded-sm relative px-3 py-1 hover:bg-gray-100">
          <button
            className="w-full text-left flex items-center outline-none focus:outline-none"
          >
            <span className="pr-1 flex-1">Навчальний процес</span>
            <span className="mr-auto">
              <svg
                className="fill-current h-4 w-4
                transition duration-150 ease-in-out"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
              >
                <path
                  d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"
                />
              </svg>
            </span>
          </button>
          <ul
            className="bg-white border rounded-sm absolute top-0 right-0
      transition duration-150 ease-in-out origin-top-left
      min-w-32
      "
          >
            <li className="px-3 py-1 hover:bg-gray-100">Графік НП</li>
            <li className="px-3 py-1 hover:bg-gray-100">Роскалд занять</li>
            <li className="px-3 py-1 hover:bg-gray-100">Про освітній процес</li>
          </ul>
        </li>
        <li className="rounded-sm relative px-3 py-1 hover:bg-gray-100">
          <button
            className="w-full text-left flex items-center outline-none focus:outline-none"
          >
            <span className="pr-1 flex-1">Студентське життя</span>
            <span className="mr-auto">
              <svg
                className="fill-current h-4 w-4
                transition duration-150 ease-in-out"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
              >
                <path
                  d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"
                />
              </svg>
            </span>
          </button>
          <ul
            className="bg-white border rounded-sm absolute top-0 right-0
      transition duration-150 ease-in-out origin-top-left
      min-w-32
      "
          >
            <li className="px-3 py-1 hover:bg-gray-100">Правила поведінки</li>
            <li className="rounded-sm relative px-3 py-1 hover:bg-gray-100">
              <button
                className="w-full text-left flex items-center outline-none focus:outline-none"
              >
                <span className="pr-1 flex-1">Навчальний процес</span>
                <span className="mr-auto">
                  <svg
                    className="fill-current h-4 w-4
                transition duration-150 ease-in-out"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                  >
                    <path
                      d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"
                    />
                  </svg>
                </span>
              </button>
              <ul
                className="bg-white border rounded-sm absolute top-0 right-0
      transition duration-150 ease-in-out origin-top-left
      min-w-32
      "
              >
                <li className="px-3 py-1 hover:bg-gray-100">Булінг</li>
              </ul>
            </li>
          </ul>
        </li>
        <li className="rounded-sm px-3 py-1 hover:bg-gray-100">Гуртожиток</li>
        <li className="rounded-sm px-3 py-1 hover:bg-gray-100">Моніторинг якості освіти</li>

      </ul>
    </div>
  </>

);

export default ButtonDropdown;

